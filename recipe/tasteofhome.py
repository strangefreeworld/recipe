"""This is the tasteofhome downlaod module"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data, clean_string

SITES_HANDLED = ("www.tasteofhome.com", "tasteofhome.com")


def recipe_list(page_url):
    page_text = recipe_html(page_url)
    soup = BeautifulSoup(page_text, 'html.parser')
    return soup.findAll("a", {"data-name":  "get recipe"} )


def download_recipe(recipe):
    """
    :param recipe: URL of recipe to download
    :return: Returns a RecipeData dataclass object of the recipe
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.select_one("h1[class='recipe-title']")
    title = title_block.string
    ingredient_block = soup.select_one("div[class='recipe-ingredients']")
    ingredients = ingredient_block.find_all('li')
    ingredient_list = [clean_string(item.get_text()) for item in ingredients]
    instructions = soup.select("li[class='recipe-directions__item']")
    instruction_list = [clean_string(item.get_text()) for item in instructions]
    time_block = soup.select_one("div[class='recipe-time-yield__label-prep']")
    if time_block is None:
        time_block = soup.select_one("div[class='total-time']")
    time = None
    if time_block is not None:
        time = time_block.get_text()
    recipe_data = create_recipe_data(title,
                                     recipe,
                                     ingredient_list,
                                     instruction_list,
                                     time)
    return recipe_data
