from bs4 import BeautifulSoup, NavigableString
from urllib.parse import urlparse
from utils import clean_string
from utils import recipe_html, create_recipe_data
import click

WORDPRESS_TITLE_CLASS = {
    "thecookierookie.com": "wprm-recipe-name wprm-block-text-normal",
    "loveandlemons.com": "wprm-recipe-name wprm-block-text-bold",
    "www.simplyquinoa.com": "wprm-recipe-name wprm-block-text-bold",
    "sweetlysplendid.com": "wprm-recipe-name wprm-block-text-bold",
    "cafedelites.com": "wprm-recipe-name wprm-block-text-uppercase",
    "lifemadesweeter.com": "wprm-recipe-name wprm-color-header"
}

WORDPRESS_HEADER_CLASS = {
    "cafedelites.com": "h1",
    "lifemadesweeter.com": "div"
}


def wordpress_rm_download(recipe):
    recipe_article = BeautifulSoup(recipe_html(recipe), "html.parser")
    title = wordpress_get_title(recipe, recipe_article)
    ingredient_list = wordpress_rm_ingredients(recipe, recipe_article)
    instruction_list = wordpress_rm_get_directions(recipe, recipe_article)
    return create_recipe_data(title, recipe, ingredient_list, instruction_list)


def wordpress_get_title(url, article):
    url_parts = urlparse(url)
    click.echo(url_parts.netloc)
    title_class = WORDPRESS_TITLE_CLASS.get(url_parts.netloc, "wprm-recipe-name")
    block_class = WORDPRESS_HEADER_CLASS.get(url_parts.netloc, "h2")
    title_block = article.find(block_class, class_=title_class)
    if url_parts.netloc != "thecookierookie.com":
        return title_block.string
    else:
        for element in title_block.children:
            if isinstance(element, NavigableString):
                return element


def wordpress_rm_get_directions(url, article):
    url_parts = urlparse(url)
    list_class = "wprm-recipe-instructions"
    instruction_list = None
    if url_parts.netloc == "therecipecritic.com":
        directions = article.select(
            'li[class="wprm-recipe-instruction"]'
        )
        instruction_list = []
        for item in directions:
            if item.string is not None:
                instruction_list.append(item.string)
            else:
                instruction_list.append(item.get_text())
    elif url_parts.netloc == "skinnysouthernrecipes.com":
        instructions = article.select('li[class="wprm-recipe-instruction"]')
        instruction_list = [clean_string(item.get_text()) for item in instructions]
    else:
        instructions = article.select('li[class="wprm-recipe-instruction"]')
        instruction_list = [clean_string(item.get_text()) for item in instructions]

    instruction_list = filter(lambda i: i is not None, instruction_list)
    unique = []
    [unique.append(x) for x in instruction_list if x not in unique]
    return unique


def wordpress_rm_ingredients(url, article):
    # url_parts = urlparse(url)
    ingredients = article.find_all("li", attrs={"class": "wprm-recipe-ingredient"})
    ingredient_list = []
    for item in ingredients:
        ingredient_list.append(item.get_text().replace("\n", " "))
    ingredient_list = filter(lambda i: i is not None, ingredient_list)
    return ingredient_list
