"""Download module for MediaVine"""
from bs4 import BeautifulSoup
from utils import recipe_html, RecipeData, clean_string, create_recipe_data

SITES_HANDLED = (
    "www.allnutritious.com",
    "allnutritious.com",
    "fromachefskitchen.com",
    "www.fromachefskitchen.com",
)


def download_recipe(recipe):
    """
    :param recipe: URL of recipe
    :return: RecipeData object
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    recipe_article = soup.find("article")
    title = get_title(recipe_article)
    ingredients = get_ingredients(recipe_article)
    directions = get_directions(recipe_article)
    time_data = get_time(recipe_article)
    return create_recipe_data(title, recipe, ingredients, directions, time_data)


def get_title(article):
    title_block = article.find(
        "h2", attrs={"class": "mv-create-title mv-create-title-primary"}
    )
    return title_block.string


def get_ingredients(article):
    ingredients_block = article.find("div", attrs={"class": "mv-create-ingredients"})
    ingredients = ingredients_block.find_all("li")
    return [clean_string(item.get_text()) for item in ingredients]


def get_directions(article):
    directions_block = article.find("div", attrs={"class": "mv-create-instructions"})
    directions = directions_block.find_all("li")
    return [clean_string(item.get_text()) for item in directions]


def get_time(article):
    prep_block = article.find(
        "div", attrs={"class": "mv-create-time mv-create-time-prep"}
    )
    cook_block = article.find(
        "div", attrs={"class": "mv-create-time mv-create-time-active"}
    )
    total_block = article.find(
        "div", attrs={"class": "mv-create-time mv-create-time-total"}
    )
    return " ".join(
        [
            clean_string(prep_block.get_text()),
            clean_string(cook_block.get_text()),
            clean_string(total_block.get_text()),
        ]
    )
