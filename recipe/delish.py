import click
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data, clean_string

SITES_HANDLED = ("www.delish.com", "delish.com")


def recipe_list(page_url):
    page_text = recipe_html(page_url)
    soup = BeautifulSoup(page_text, 'html.parser')
    slideshow_container = soup.select_one('div[class="slideshow-outer"]')
    return [link for link in (item.find("a") for item in slideshow_container.select('div[class="slideshow-slide-dek"]')) if link]


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.select_one('h1[class="content-hed recipe-hed"]') # "h1", class_="content-hed recipe-hed")
    title = title_block.string
    ingredient_items = soup.select('div[class="ingredient-item"]')
    ingredient_list = [
        clean_string(" ".join(item.get_text().split())) for item in ingredient_items
    ]
    directions_div = soup.select_one('div[class="direction-lists"]')
    instructions = directions_div.select("li")
    instruction_list = [clean_string(item.get_text()) for item in instructions]
    return create_recipe_data(title, recipe, ingredient_list, instruction_list)
