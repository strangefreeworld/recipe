from bs4 import BeautifulSoup
from utils import recipe_html, clean_string, create_recipe_data


SITES_HANDLED = ('www.foodnetwork.com', 'foodnetwork.com')


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), 'html.parser')
    recipe_section = soup.find('section', class_="o-Recipe")
    title = foodnetwork_get_title(recipe_section)
    ingredient_list = foodnetwork_get_ingredients(recipe_section)
    instruction_list = foodnetwork_get_directions(recipe_section)
    time_info = foodnetwork_get_time(recipe_section)
    time_info = ' '.join(time_info.split())
    recipe = create_recipe_data(title,
                                recipe,
                                ingredient_list,
                                instruction_list,
                                time_info)
    return recipe


def foodnetwork_get_title(section):
    title_block = section.find('h1', class_="o-AssetTitle__a-Headline")
    return clean_string(title_block.get_text())


def foodnetwork_get_ingredients(section):
    all_ingredients = section.find_all('input', class_="o-Ingredients__a-Ingredient--Checkbox")
    return [clean_string(element.get('value')) for element in all_ingredients]


def foodnetwork_get_directions(section):
    instructions = section.find_all('li', class_="o-Method__m-Step")
    return [clean_string(item.get_text()) for item in instructions]

def foodnetwork_get_time(section):
    time_list = section.find('ul', class_="o-RecipeInfo__m-Time")
    return time_list.get_text() if time_list else None
