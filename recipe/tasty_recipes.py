from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data


SITES_HANDLED = (
    "acouplecooks.com",
    "www.acouplecooks.com",
    "gimmesomeoven.com",
    "www.gimmesomeoven.com",
    "www.greenevi.com",
    "greenevi.com",
    "cookieandkate.com",
    "www.cookieandkate.com"
)


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    recipe_article = soup.find("article")
    title = get_title(recipe_article)
    ingredient_list = get_ingredients(recipe_article)
    instruction_list = get_instructions(recipe_article)
    time = get_time(recipe_article)
    return create_recipe_data(title, recipe, ingredient_list, instruction_list, time)


def get_title(article):
    """
    :param article: Article element containing recipe
    :return: Title of recipe
    """
    title_block = article.select_one("h2[class=tasty-recipes-title]")
    if not title_block:
        title_block = article.select_one("h2")
    return title_block.get_text()


def get_ingredients(article):
    """
    :param article: Article element containing recipe
    :return: List of ingredients
    """
    ingredient_div = article.select_one("div[class=tasty-recipes-ingredients-body]")
    if ingredient_div is None:
        ingredient_div = article.select_one("div[class=tasty-recipes-ingredients]")
    if ingredient_div is None:
        ingredient_div = article.select_one("div[class=tasty-recipe-ingredients]")
    ingredients = ingredient_div.select("li")
    return [item.get_text() for item in ingredients]


def get_instructions(article):
    """
    :param article: Article element containing recipe
    :return: List of instructions
    """
    instruction_div = article.select_one("div[class=tasty-recipes-instructions-body]")
    if instruction_div is None:
        instruction_div = article.select_one("div[class=tasty-recipes-instructions]")
    if instruction_div is None:
        instruction_div = article.select_one("div[class=tasty-recipe-instructions]")
    instructions = instruction_div.select("li")
    return [item.get_text() for item in instructions]

def get_time(article):
    """
    :param article: Article element containing recipe
    :return: Time String
    """
    prep_time = article.select_one("span[class='tasty-recipes-prep-time']").get_text()
    cook_time = article.select_one("span[class='tasty-recipes-cook-time']").get_text()
    total_time = article.select_one("span[class='tasty-recipes-total-time']").get_text()
    return f"Prep Time: {prep_time}  Cook Time: {cook_time}  Total Time: {total_time}"

