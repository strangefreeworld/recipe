"""Utilities functions for the parser"""
import urllib.robotparser as urobot
from urllib.parse import urlparse
import unicodedata
from collections import namedtuple
from dataclasses import dataclass, field
import requests
import click


def check_url(recipe_url):
    """Some browsers don't add the http which blows things up."""
    url_parts = urlparse(recipe_url)
    updated_url = None
    if url_parts.scheme is None or len(url_parts.scheme) == 0:
        updated_url = f"https://{recipe_url}"
    return updated_url if updated_url is not None else recipe_url

def check_download(recipe_url):
    """
    This is a workaround for the built in robots parser. It was failing on some sites.
    :param recipe_url: URL of recipe
    :return: Boolean result saying if you are allowed to download.
    """
    url_parts = urlparse(recipe_url)
    robots_file = f"{url_parts.scheme}://{url_parts.netloc}/robots.txt"
    robot_parser = urobot.RobotFileParser()
    # The robots.txt file is downloaded manually because one of the websites
    # I was testing with (budgetbytes.com) failed in using the built in
    # robot downloader, which appears to use
    robot_text = requests.get(robots_file)
    robot_parser.parse(robot_text.text.splitlines())
    return robot_parser.can_fetch("test", recipe_url)


def recipe_html(url_recipe):
    """
    This creates HTTP headers and gets the recipe page. Some sites needed this
    to respond.
    :param url_recipe: URL of recipe
    :return: Text of the HTML page
    """
    headers = {
        "Accept-Language": "en-US, en;q=0.5",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36",
    }
    result = requests.get(url_recipe, headers=headers)
    return result.text.replace("\xa0", "")


def clean_url(url):
    """
    This removes parameters from the URL
    :param url: Text of URL
    :return: URL with parameters removed
    """
    return url.split("?")[0]


def exists(item):
    """
    :param item:
    :return: Boolean if the item exists
    """
    return item is not None


def clean_string(text):
    """
    This cleans non-breaking spaces from the string
    :param text: Text to clean
    :return: Cleaned string
    """
    clean_text = text.lstrip().rstrip()
    if "\n" in clean_text:
        clean_text = clean_text.replace("\n", "")
    return unicodedata.normalize("NFKD", clean_text)


RecipeData = namedtuple("RecipeData", "name url ingredients directions")


@dataclass
class RecipeInformation:
    """
    This is a new class to represent the recipe information.
    """
    url: str
    name: str
    ingredients: list
    directions: list
    recipe_time: str


def create_recipe_data(name,
                       url,
                       ingredients,
                       directions,
                       recipe_total_time=None):
    return RecipeInformation(name=name,
                      url=url,
                      ingredients=ingredients,
                      directions=directions,
                      recipe_time=recipe_total_time)


def recipe_title(url):
    url_parts = url.split('/')
    base_title = url_parts[-1]
    if base_title is None or len(base_title) == 0:
        base_title = url_parts[-2]
    if base_title.endswith("-recipe"):
        base_title = base_title.replace("-recipe", "")
    return base_title


def uri_validator(x):
    try:
        result = urlparse(x)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False
