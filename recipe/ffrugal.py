"""Parsing module for fbulesslyfrugal.com"""
from bs4 import BeautifulSoup
from utils import recipe_html, RecipeData, clean_string

SITES_HANDLED = ("www.fabulesslyfrugal.com", "fabulesslyfrugal.com")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe to dwonload
    :return: RecipeData object
    """
    soup = BeautifulSoup(recipe_html(recipe), 'html.parser')
    article = soup.find('div', class_="recipe-box")
    title = frugal_title(soup)
    ingredients = frugal_ingredients(article)
    instructions = frugal_instructions(article)
    recipe = RecipeData(name=title,
                        url=recipe,
                        ingredients=ingredients,
                        directions=instructions)
    return recipe


def frugal_title(container):
    """
    :param container: BeautifulSoup object containing title block
    :return: Recipe title string
    """
    title_block = container.find('h1', class_="title")
    return title_block.string


def frugal_ingredients(article):
    """
    :param article: Article object containing recipe information
    :return: List of ingredients
    """
    ingredient_list = article.find_all('li', itemprop="recipeIngredient")
    return [clean_string(item.string) for item in ingredient_list]


def frugal_instructions(article):
    """
    :param article: Article object containing information
    :return: List containing instructions
    """
    instruction_list = article.find_all('li', itemprop="recipeInstructions")
    return [clean_string(item.string) for item in instruction_list]
