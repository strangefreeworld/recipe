"""This module implements downloading fromm eatingwell.com"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data

SITES_HANDLED = ("www.eatingwell.com", "eatingwell.com")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe
    :return: RecipeData object
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.find(
        "h1", attrs={"class": "headline heading-content elementFont__display"}
    )
    title = title_block.string
    ingredients = soup.find_all(
        "span", attrs={"class": "ingredients-item-name elementFont__body"}
    )
    ingredient_list = [item.get_text() for item in ingredients]
    instructions = soup.find_all(
        "div",
        attrs={
            "class": "section-body elementFont__body--paragraphWithin elementFont__body--linkWithin"
        },
    )
    instruction_list = [item.get_text() for item in instructions]
    return create_recipe_data(title, recipe, ingredient_list, instruction_list)
