import click
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data, clean_string


SITES_HANDLED = ("www.inspiredtaste.net", "inspiredtaste.net")


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.select_one('div[class="itr-recipe-title"]')
    title = title_block.get_text()
    click.echo(title)
    ingredient_block = soup.select_one('div[class="itr-ingredients"]')
    ingredients = [item.get_text() for item in ingredient_block]
    directions_block = soup.select_one('div[class="itr-directions"]')
    directions = [item.get_text() for item in directions_block]
    return create_recipe_data(title, recipe, ingredients, directions)


