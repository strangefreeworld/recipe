"""This will write out the recipe in Markdown format"""


def write_recipe_markdown(markdown_file, recipe_data):
    """
    :param markdown_file: File to write Markdown to
    :param recipe_data: RecipeData object
    :return: None
    """
    recipe_lines = list()
    recipe_lines.append(f"# [{recipe_data.name}]({recipe_data.url})")
    if recipe_data.recipe_time is not None:
        recipe_lines.append(f"\n### Time: {recipe_data.recipe_time}")
    recipe_lines.append("\n## Ingredients\n")
    for ingredient in recipe_data.ingredients:
        recipe_lines.append(f" * {ingredient}")
    recipe_lines.append("\n## Instructions\n")
    num = 1
    for direction in recipe_data.directions:
        recipe_lines.append(f" {num}. {str(direction)}")
        num += 1
    with open(markdown_file, "w") as mdfile:
        mdfile.write("\n".join(recipe_lines))
