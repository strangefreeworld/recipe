"""This is the structured project template downlaod module"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data, clean_string

SITES_HANDLED = ("www.thespruceeats.com", "thespruceeats.com")


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.find("h1", attrs={"class": "heading__title"})
    title = title_block.string
    ingredient_block = soup.find("section", id="section--ingredients_1-0")
    ingredients = ingredient_block.find_all("li")
    ingredient_list = [clean_string(item.get_text()) for item in ingredients]
    instruction_block = soup.find(
        "ol",
        attrs={
            "class": "comp mntl-sc-block-group--OL mntl-sc-block mntl-sc-block-startgroup"
        },
    )
    instructions = instruction_block.find_all(
        "p", attrs={"class": "comp mntl-sc-block mntl-sc-block-html"}
    )
    #    instructions = soup.find_all('p', attrs={'class': 'comp mntl-sc-block mntl-sc-block-html'})
    instruction_list = [clean_string(item.get_text()) for item in instructions]
    time_block = soup.find("div", attrs={"class": "project-meta__times-container"})
    time_text = clean_string(time_block.get_text())
    recipe = create_recipe_data(
        title, recipe, ingredient_list, instruction_list, time_text
    )
    return recipe
