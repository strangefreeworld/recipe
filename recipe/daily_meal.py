"""Download module for dailymeal.com"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data

SITES_HANDLED = ("www.thedailymeal.com", "thedailymeal.com")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe
    :return: RecipeData object
    """
    soup = BeautifulSoup(recipe_html(recipe), 'html.parser')
    title_block = soup.find('h1')
    title = title_block.get_text()
    ingredient_div = soup.find('div', attrs={'class': 'ingredients section-body'})
    ingredient_list_items = ingredient_div.find_all('li')
    ingredient_list = [item.get_text() for item in ingredient_list_items]
    instruction_div = soup.find('div', attrs={'class': 'recipe-preparation section-body'})
    instructions = instruction_div.find_all('p')
    instruction_list = [item.get_text() for item in instructions]
    recipe = create_recipe_data(title,
                                     recipe,
                                     ingredient_list,
                                     instruction_list)
    return recipe
