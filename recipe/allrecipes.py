from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data


SITES_HANDLED = ("www.allrecipes.com", "allrecipes.com")

def download_recipe(recipe):
    """
    :param recipe: URL of recipe
    :return: RecipeData object
    """
    ingredient_list = []
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.find('h1', attrs={"class": "headline heading-content"})
    if not title_block:
        title_block = soup.find('h1', attrs={"class": "headline heading-content elementFont__display"})
    title = title_block.string
    ingredient_spans = soup.find_all('span', attrs={"class": "ingredients-item-name"})
    for item in ingredient_spans:
        ingredient_list.append(item.get_text(strip=True))
    instructions = soup.find_all('li', attrs={"class": "subcontainer instructions-section-item"})
    instruction_list = []
    for instruction_item in instructions:
        instruction_list.append(instruction_item.div.get_text(strip=True))
    return create_recipe_data(title,
                              recipe,
                              ingredient_list,
                              instruction_list)
