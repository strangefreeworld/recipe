import sys
from pathlib import Path

__version__ = '0.3.0'
__app_name__ = "recipe"
__app_author__ = "strangefreeworld"

module_path = str(Path(__file__).absolute().parents[0])
if module_path not in sys.path:
    sys.path.append(module_path)
