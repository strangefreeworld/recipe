"""This module implements the download for Easy Recipe formatted sites"""
from bs4 import BeautifulSoup
from utils import exists, recipe_html, create_recipe_data

SITES_HANDLED = ("www.melaniecooks.com", "melaniecooks.com")


def download_recipe(article):
    """This is the entry point function"""
    article_text = recipe_html(article)
    soup = BeautifulSoup(article_text, "html.parser")
    title = easyrecipe_get_title(soup)
    ingredient_list = easyrecipe_ingredients(soup)
    instruction_list = easyrecipe_instructions(soup)
    recipe_data = create_recipe_data(title, article, ingredient_list, instruction_list)
    return recipe_data


def easyrecipe_get_title(article):
    """This function gets the title of the recipe"""
    title_block = article.find("h2", attrs={"class": "ERSName"})
    if title_block is None:
        title_block = article.find("div", attrs={"class": "ERSName"})
    return title_block.string


def easyrecipe_ingredients(article):
    """This function gets the ingredients in the recipe"""
    ingredients_block = article.find("div", class_="ERSIngredients")
    all_ingredients = ingredients_block.find_all("li")
    ingredient_list = [
        item.get_text() for item in all_ingredients if exists(item.get_text())
    ]
    return ingredient_list


def easyrecipe_instructions(article):
    """This function gets the instructions for the recipe"""
    instruction_container = article.find("div", class_="ERSInstructions")
    instruction_items = instruction_container.find_all("li")
    instruction_list = [
        item.get_text() for item in instruction_items if exists(item.get_text())
    ]
    return instruction_list
