import argparse
import logging
from urllib.parse import urlparse

from utils import check_download, clean_url
from build_downloader import download_builder
from wordpress import wordpress_rm_download
import recipe_writer

PARSER = argparse.ArgumentParser(prog="recipe",
                                 description="Extract a recipe web page to a Markdown file")
PARSER.add_argument("url", help="URL of recipe to download")
PARSER.add_argument("markdown", help="Name of output Markdown file")

if __name__ == '__main__':
    logging.basicConfig(filename="recipe.log",
                        level=logging.INFO,
                        format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s")
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logging.getLogger('').addHandler(console)
    args = PARSER.parse_args()
    recipe_download_function = download_builder()
    cleaned_url = clean_url(args.url)
    if check_download(cleaned_url):
        logging.info("Getting")
        url_parts = urlparse(cleaned_url)
        logging.info(url_parts.netloc)
        recipe_information = recipe_download_function.get(url_parts.netloc,
                                                          wordpress_rm_download)(cleaned_url)
        recipe_writer.write_recipe_markdown(args.markdown, recipe_information)
