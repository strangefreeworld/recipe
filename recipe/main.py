import os.path

import click
from urllib.parse import urlparse
from utils import check_download, clean_url, recipe_title, uri_validator
import pyperclip
from build_downloader import download_builder
from wordpress import wordpress_rm_download as wp_default
from recipe_writer import write_recipe_markdown
import tasteofhome
import delish
import app_config
import toml


@click.group(invoke_without_command=True)
@click.pass_context
def process(ctx):
    if ctx.invoked_subcommand is None:
        file()


@process.command()
@click.argument("folder", type=click.Path())
def config(folder):
    click.echo(f"Path {folder} configured")
    app_config.check_config_dir()
    config_data = {"path": folder}
    with open(app_config.config_file_path("config.toml"), "w") as config_file:
        toml.dump(config_data, config_file)


@process.command()
def batch():
    original_link = pyperclip.paste()
    click.echo(f"Original link: {original_link}")
    cleaned_url = clean_url(original_link)
    if uri_validator(cleaned_url) and check_download(cleaned_url):
        url_parts = urlparse(cleaned_url)
        link_function = None
        recipe_function = None
        if "tasteofhome.com" in cleaned_url:
            link_function = tasteofhome.recipe_list
            recipe_function = tasteofhome.download_recipe
        elif "delish.com" in cleaned_url:
            link_function = delish.recipe_list
            recipe_function = delish.download_recipe

        links = link_function(cleaned_url)
        for link in links:
            recipe = link.get('href')
            if "https" not in recipe or "http" not in recipe:
                recipe = f"{url_parts.scheme}://{url_parts.netloc}/{recipe}"
            recipe = clean_url(recipe)
            click.echo(recipe)
            try:
                recipe_data = recipe_function(recipe)
                filename = recipe_title(recipe)
                output_folder = app_config.recipe_path()
                output_file = os.path.join(output_folder, f"{filename}.md")
                click.echo(f"Saving {output_file}")
                write_recipe_markdown(output_file, recipe_data)
            except AttributeError as _:
                click.echo("Error occurred")
    else:
        click.echo("Not a valid URL")


@process.command()
def file():
    original_link = pyperclip.paste()
    download_function = download_builder()
    cleaned_url = clean_url(original_link)
    if uri_validator(cleaned_url) and check_download(cleaned_url):
        url_parts = urlparse(cleaned_url)
        click.echo(f"Recipe link: {cleaned_url}")
        output_filename = recipe_title(cleaned_url)
        click.echo(f"File name: {output_filename}")
        recipe_data = download_function.get(url_parts.netloc, wp_default)(cleaned_url)
        output_folder = app_config.recipe_path()
        full_output_path = os.path.join(output_folder, f"{output_filename}.md")
        click.echo(f"Writing output file {full_output_path}")
        write_recipe_markdown(full_output_path, recipe_data)
    else:
        click.echo("Not a valid url")
