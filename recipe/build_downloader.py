"""
This module will build the module downloader dict.
It does this by performing a dir on available modules.
If those modules are correctly set up for being a downloader
module, they will be added to the dict
"""
import sys
from pathlib import Path


def download_builder():
    downloaders = {}
    working_dir = Path.cwd()
    module_names = [f.stem for f in working_dir.glob("*.py")]
    for module_name in module_names:
        current_module = None
        if module_name in sys.modules:
            current_module = sys.modules[module_name]
        else:
            current_module = __import__(module_name)
        item_key = getattr(current_module, "SITES_HANDLED", None)
        item_function = getattr(current_module, "download_recipe", None)
        check_tuple = (item_key, item_function)
        if all(check_tuple):
            for key in item_key:
                downloaders[key] = item_function
    return downloaders
