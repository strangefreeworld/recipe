"""Download module for Recipe Press -- right now healthiersteps"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data

SITES_HANDLED = ("www.healthiersteps.com", "healthiersteps.com")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe
    :return: RecipeData object
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    recipe_article = soup.select("article")
    title = get_title(recipe_article)
    ingredient_list = get_ingredients(recipe_article)
    instruction_list = get_directions(recipe_article)
    recipe = create_recipe_data(title, recipe, ingredient_list, instruction_list)
    return recipe


def get_title(article):
    """
    Returns the title of the recipe
    :param article: Text of web page
    :return: String of recipe title
    """
    title_element = article.select_one("h1[class=post-title post-title-full]")
    return title_element.string


def get_ingredients(article):
    """
    Creates an ingredient list from the
    :param article: Text of web page
    :return: List of ingredients
    """
    ingredients = article.select("li[class=rpr-ingredient]")
    return [item.get_text() for item in ingredients]


def get_directions(article):
    """
    Generates the ingredient list
    :param article: Text of web page
    :return: List of instructions
    """
    directions = article.select("li[class=rpr-instruction]")
    return [item.get_text() for item in directions]
