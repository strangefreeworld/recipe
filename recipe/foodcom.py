from bs4 import BeautifulSoup
from utils import recipe_html, RecipeData, clean_string

SITES_HANDLED = ("www.food.com", "food.com")


def download_recipe(recipe):
    soup = BeautifulSoup(recipe_html(recipe), 'html.parser')
    article = soup.find('article')
    title = foodcom_title(article)
    ingredients = foodcom_ingredients(article)
    instructions = foodcom_instructions(article)
    recipe = RecipeData(name=title,
                        url=recipe,
                        ingredients=ingredients,
                        directions=instructions)
    return recipe


def foodcom_title(article):
    title_block = article.find('h1', class_="amp-recipe-title")
    return title_block.string


def foodcom_ingredients(article):
    ingredient_list = article.find('ul', class_="gk-amp-ingredients")
    return [clean_string(item.get_text()) for item in ingredient_list.find_all('li')]


def foodcom_instructions(article):
    instructions = article.find_all('li', class_="gk-amp-direction-item")
    return [clean_string(item.get_text()) for item in instructions]