"""This is the damndelicious downlaod module"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data

SITES_HANDLED = ("www.damndelicious.net", "damndelicious.net")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe to download
    :return: Returns a RecipeData dataclass object of the recipe
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.find('h1', attrs={"class": "post-title"})
    title = title_block.string
    ingredient_block = soup.find('div', attrs={"class": "ingredients"})
    ingredient_list_block = ingredient_block.find('ul')
    all_ingredients = ingredient_list_block.find_all("li")
    print(all_ingredients)
    ingredient_list = [item.get_text() for item in all_ingredients]
    instruction_block = soup.find('div', attrs={"class": "instructions"})
    instruction_block_list = instruction_block.find("ol")
    all_instructions = instruction_block_list.find_all("li")
    instruction_list = [item.get_text() for item in all_instructions]
    recipe_data = create_recipe_data(title,
                                     recipe,
                                     ingredient_list,
                                     instruction_list)
    return recipe_data
