"""This is the launcher stub for serenity"""
import os
import sys
from pathlib import Path


def launch_app():
    module_path = str(Path(__file__).absolute().parents[0])
    if module_path not in sys.path:
        sys.path.append(module_path)
    os.chdir(module_path)
    from main import process
    process()
