"""This is the downloader for loveandlemons.com It appears to have two
   different formats for its recipe articles
"""

from bs4 import BeautifulSoup
import easyrecipe
import wordpress
from utils import RecipeData, recipe_html

SITES_HANDLED = ("www.loveandlemons.com", "loveandlemons.com")


def download_recipe(recipe):
    article_text = recipe_html(recipe)
    using_easyrecipe = "wprm-recipe-container" not in article_text
    soup = BeautifulSoup(article_text, "html.parser")
    title = (
        easyrecipe.easyrecipe_get_title(soup)
        if using_easyrecipe
        else wordpress.wordpress_get_title(recipe, soup)
    )
    ingredient_list = (
        easyrecipe.easyrecipe_ingredients(soup)
        if using_easyrecipe
        else wordpress.wordpress_rm_ingredients(recipe, soup)
    )
    instruction_list = (
        easyrecipe.easyrecipe_instructions(soup)
        if using_easyrecipe
        else wordpress.wordpress_rm_get_directions(recipe, soup)
    )
    recipe = RecipeData(
        name=title, url=recipe, ingredients=ingredient_list, directions=instruction_list
    )
    return recipe
