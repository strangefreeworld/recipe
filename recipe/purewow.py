"""This is the purewow downlaod module"""
from bs4 import BeautifulSoup
from utils import recipe_html, create_recipe_data

SITES_HANDLED = ("www.purewow.com", "purewow.com")


def download_recipe(recipe):
    """
    :param recipe: URL of recipe to download
    :return: Returns a RecipeData dataclass object of the recipe
    """
    soup = BeautifulSoup(recipe_html(recipe), "html.parser")
    title_block = soup.find('h1', attrs={"class": "headline recipe-mobile"})
    title = title_block.string
    time_block = soup.find("div", attrs={"class": "cook-time-serving"})
    time_text = time_block.get_text()
    print(title)
    print(" ".join(time_text.split()))
    ingredient_block = soup.find("div", attrs={"class": "recipe-list ingreds recipe-instructions"})
    ingredient_list = [item.get_text() for item in ingredient_block.descendants]
    print(set(ingredient_list))
    instruction_block = soup.find("div", attrs={"class": "recipe-instructions"})
    instruction_list = [item.get_text() for item in instruction_block.descendants]
    print(instruction_list)
    return None
